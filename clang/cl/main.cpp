#include <iostream>
#include <library.h>
#include <cHebbLearn.h>
#include <cstring>
#include "HebbianTests.h"
#include "BidirPerceptronTests.h"
#include "MathFuncTests.h"
#include "KMeansTests.h"


int main() {

    //mathTests();

    //hebTester();
    //bdirpercTestMain();
    kmeansTestMain();

    printf("\nDone\n");
    return 0;
}

