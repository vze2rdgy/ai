//
// Created by santony on 8/22/20.
//

#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <cHebbLearn.h>

void TestHebbian(cHebbLearn& hebb, float testrec[], int numAttrs);

int hebTester() {

    cHebbLearn hebb(16);

    constexpr int NUMATTRS = 16;
    constexpr int ROWS = 2;
    float records[][NUMATTRS] = {
            {
                    -1,  1,  1, -1,
                    1, -1, -1,  1,
                    1, -1, -1,  1,
                    -1,  1,  1, -1
            },
            {
                    -1, -1, -1, -1,
                    -1,  1,  1, -1,
                    -1,  1,  1, -1,
                    -1, -1, -1, -1
            }
    };
    // setup data
    float **fitData = new float*[ROWS];
    size_t totbytes = NUMATTRS * sizeof(float);
    for (int i=0; i<ROWS; i++)
    {
        fitData[i] = new float[NUMATTRS];
        memcpy(fitData[i], records[i], totbytes);
    }

    hebb.fit((float**)fitData, 2);

    float testData1[] = {-1, -1, -1, -1,
                         -1,  1,  1, -1,
                         1,  1,  1,  1,
                         1,  1,  1,  1 };

    TestHebbian(hebb, testData1, NUMATTRS);

    float testData2[] = { 1,  1,  1,  1,
                          1,  1,  1,  1,
                          1, -1, -1,  1,
                          -1,  1,  1, -1 };

    TestHebbian(hebb, testData2, NUMATTRS);

    for (int i=0; i<ROWS; i++)
        delete [] fitData[i];
    delete [] fitData;

    return 0;
}

#define XLATE(x)	((x > 0.0) ? ' ' : '*')

void TestHebbian(cHebbLearn& hebb, float testrec[], int numAttrs)
{
    auto outputs = hebb.predict(testrec);

    for (auto i = 0; i < numAttrs; i+=4)
    {
        printf("%c %c %c %c -- %c %c %c %c\n",
               XLATE(testrec[i+0]), XLATE(testrec[i+1]),
               XLATE(testrec[i+2]), XLATE(testrec[i+3]),
               XLATE(outputs[i+0]), XLATE(outputs[i+1]),
               XLATE(outputs[i+2]), XLATE(outputs[i+3]) );
    }

    printf("\n");

}