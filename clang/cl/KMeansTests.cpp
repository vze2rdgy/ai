//
// Created by santony on 12/19/20.
//

#include <KMeans.h>
#include "KMeansTests.h"
#include <iostream>
#include <MathFunctions.h>

using Record = KMeans::FeatureRecord;

constexpr int k = 7;
constexpr int cFeatures = 3;
constexpr int cRecords = 10;
const char** Groups = new char const*[cFeatures]{"First", "Second", "Third"};
const char* b = "ABC";

int compare(const void* x, const void* y) {
    const auto g1 = ((Record *) x)->group;
    const auto g2 = ((Record *) y)->group;
    auto ret = g1 - g2;
    ret = ret > 0 ? 1 : ret < 0 ? -1 : 0;
    return ret;
}

int kmeansTestMain()
{
    RANDINIT();
    printf("---------------------------------------------------\n");
    // Prepare training data.
    Record records[cRecords];
    for (auto x = 0; x < cRecords; x++) {
        records[x].features = new double[cFeatures];
        for (auto y = 0; y < cFeatures; y++) {
            records[x].features[y] = (double) (RANDMAX(1000) + 1);
        }
    }

    KMeans km(cFeatures, k);
    km.Train(records, cRecords);
    printf("\nTraining Data:\n");

    // sort by cluster id
    qsort(records, cRecords, sizeof(Record), compare);


    for (auto x = 0; x < cRecords; x++) {
        for (auto y = 0; y < cFeatures; y++) {
            printf("%.2f ", records[x].features[y]);
        }
        auto cluster = records[x].group; // (int) km.getCluster(x);
        printf(" | %d \n", cluster);
    }

    printf("\nPredicted Result:\n");
    Record record;
    for (auto y = 0; y < cFeatures; y++) {
        record.features = new double[cFeatures];
        record.features[y] = (double) (RANDMAX(cFeatures) + 1);
        printf("%.2f ", record.features[y]);
    }
    int predictedValue = km.Predict(record).getPredictedValue();
    printf(" | %d \n", predictedValue);
    printf("---------------------------------------------------\n");
    return 0;
}