//
// Created by santony on 8/22/20.
//

#include "BidirPerceptronTests.h"
#include <cBidirPerceptron.h>
#include <stdio.h>

constexpr size_t INPUTS = 2;

//double (*array)[INPUTS+1] = new double[2][INPUTS+1]{
//        {
//                2., 3., 1., 4., 7., 1.
//        },
//        {
//                3., 4., 2., 9., 6., 0.
//        }
//};

double (*array)[INPUTS+1] = new double[2][INPUTS+1]{
        {
                2., 3., 1.
        },
        {
                3., 4., 0.
        }
};

// we can also use a c++ lambda inside test main function.
// for optimization, I am avoiding all modern c++ stuff.
const double &fgetCellValue(size_t row, size_t cell)
{
    return array[row][cell];
}

int bdirpercTestMain() {

    cBidirPerceptron perc(INPUTS, 1, 2, 2, 0.0);

    //auto fgetCellValue = [&](size_t r, size_t c)->double {return array[r][c];};

    //double *predict = new double[INPUTS]{3., 4., 2., 9., 6.};
    double *predict = new double[INPUTS]{2., 3.};

    perc.Train(2, fgetCellValue).Predict(predict); // TODO redesign using a template class so that array sizes are templated.

    printf("Predicted Value: %.2f", perc.getPredictedResult());

    delete[] predict;

    return 0;
}
