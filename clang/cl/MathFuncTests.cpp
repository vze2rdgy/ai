//
// Created by santony on 8/22/20.
//

#include <MathFunctions.h>
#include "MathFuncTests.h"
#include <stdio.h>

int mathTests()
{
    for (double i=-10; i<11; i++)
    {
        printf("SigmodE: %.4f -> %.4f\n", i, SigmoidE(i));
        printf("Tangent: %.4f -> %.4f\n", i, Tangent(i));
    }

    return 0;
}