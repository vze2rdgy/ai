//
// Created by sajia on 8/29/2020.
//

#ifndef CLANG_CDATAMATRIX_H
#define CLANG_CDATAMATRIX_H

#include <stdlib.h>

/*
 * A handy two dimensional array holding class. Easier to pass
 * into various AI classes we have here.
 */
template<size_t ROWS, size_t COLS>
class cDataMatrix {

public:
    double Array[ROWS][COLS];

public:
    cDataMatrix(){

    }

    void setRow(double data[ROWS][COLS])
    {

    }


};


#endif //CLANG_CDATAMATRIX_H
