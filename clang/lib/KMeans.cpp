//
// Created by santony on 12/18/20.
//

#include <cmath>
#include "KMeans.h"

KMeans::KMeans(const int featureCount, const int kclusters, const bool autoCluster):
_k(kclusters), _cFeatures(featureCount), _autoCluster(autoCluster)
{
}

KMeans::~KMeans() {
    _Term();
}

const KMeans& KMeans::Train(FeatureRecord *records, long numberOfRecords) {
    // ensure clean memory state.
    _Term();
    // k cannot be greater than number of records. Set to that value
    // in case user provided a big k value (see constructor).
    if (_k > numberOfRecords)
        _k = numberOfRecords;

    _cRecords = numberOfRecords;
    _records = records;
    _initKMeans();
    for (auto i=0; i<_k; i++) {
        _assignCentroids(i);
    }
    _beginClustering();
    return *this;
}

void KMeans::_initKMeans() {
    _classes = new double[_cRecords];
    for (auto i=0; i<_cRecords; i++)
    {
        if (i<_k) // to revisit
        {
            _classes[i] = (double)i;
            _records[i].group = i;
        }
        else
        {
            _classes[i] = -1.;
            _records[i].group = -1;
        }
    }
    _centroids = new double* [_k];
    for (auto i=0; i<_k; i++)
    {
        _centroids[i] = new double [_cFeatures];
        for (auto j=0; j<_cFeatures; j++)
        {
            _centroids[i][j] = 0.;
        }
    }
}

void KMeans::_assignCentroids(int cluster) {

    long counter = 0;
    for (auto p=0; p<_cRecords; p++)
    {
        const double& kclass = _classes[p];
        if (kclass == cluster)
        {
            for (auto f=0; f<_cFeatures; f++)
            {
                const double& fv = _records[p].features[f];
                _centroids[cluster][f] += fv;
            }
            counter++;
        }
    }

    // store the average of the centroid.
    for (auto f=0; f<_cFeatures; f++)
    {
        double& clval = _centroids[cluster][f];
        clval /= (double)counter;
    }
}

void KMeans::_beginClustering() {
    bool keeprunning = true;
    while (keeprunning)
    {
        keeprunning = false;
        for (auto p=_cRecords-1;p>=0;p--)
        {
            auto nearest = _detectNearestCentroid(p);
            long prev;
            if (nearest != (prev = _classes[p])) {
                _classes[p] = nearest;
                _records[p].group = (int)nearest;

                if (prev != -1) {
                    _assignCentroids(prev);
                }
                _assignCentroids(nearest);
                keeprunning = true;
            }
        }
    }
}


long KMeans::_detectNearestCentroid(long recordIndex) {
    double dClosest = (double)(1 << 30);
    long lClosestCluster = 0;
    for (auto c=0, b=0; c < _k; c++)
    {
        double dCurr = _measureDistance(recordIndex, c);
        if (dCurr < dClosest)
        {
            lClosestCluster = c;
            dClosest = dCurr;
        }
    }
    return lClosestCluster;
}

double KMeans::_measureDistance(long recordIndex, long clusterIndex) {
    double dDist = 0.;
    for (auto f=0; f<_cFeatures;f++)
    {
        const double d = pow((_records[recordIndex].features[f] - _centroids[clusterIndex][f]), 2);
        dDist += d;
    }
    return sqrt(dDist);
}

const KMeans & KMeans::Predict(const FeatureRecord &record) const {

    return *this;
}

const double KMeans::getCluster(long recordIndex) const {
    if (recordIndex < 0 || recordIndex >= _cRecords)
        return -1;

    return _classes[recordIndex];
}

const double KMeans::getPredictedValue() const {
    return _predictedCluster;
}

void KMeans::_Term() {
    if (_centroids)
    {
        // delete all centroid data.
        for (long i=0; i<_k; i++)
        {
            delete[] _centroids[i];
            _centroids[i] = nullptr;
        }
        delete[] _centroids;
        _centroids= nullptr;
    }
    _centroids = nullptr;

    // delete internal classification
    if (_classes) {
        delete[] _classes;
        _classes = nullptr;
    }

    // erasing records memory is class user's responsibility.
    _cRecords = 0;
    _records = nullptr;
}