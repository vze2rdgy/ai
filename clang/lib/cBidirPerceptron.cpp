//
// Created by santony on 8/20/20.
//

/*
 * Notes:
 * SOFTMAX used Neural networks (see wiki)
 *  The softmax function is often used in the final layer of a neural network-based classifier.
 *  Such networks are commonly trained under a log loss (or cross-entropy) regime,
 *  giving a non-linear variant of multinomial logistic regression.
 *  https://en.wikipedia.org/wiki/Softmax_function
 */


#include <cstring>
#include "cBidirPerceptron.h"
#include "MathFunctions.h"
#include <stdio.h>

#define RAND_WEIGHT (((double)rand() / (double)RAND_MAX))

#define CANPRINTMEM

FACTIVATOR activators[][2] = {
        { &SigmoidE, &SigmoidED },
        { &Tangent, &TangentD }
};

FERROR errorFunctions[] = {
        &SquaredError,
};

#ifdef CANPRINTMEM
#define PRINTMEM(arr, size)                         \
    printf("Cells in %s:\n", #arr);                  \
    for (long cell = 0; cell < size; cell++ )     \
        printf("cell[%zd]=%.6f\n", cell, arr[cell]);
#else
#define PRINTMEM(arr, size)
#endif

cBidirPerceptron::cBidirPerceptron(int numInputs, int numOutputs,
                                   int numHiddenNeurons, int numHiddenLayers,
                                   double tolerance, int epoch,
                                   const double rho) :
        _numInputs(numInputs),
        _numOutputs(numOutputs),
        _numHiddenNeurons(numHiddenNeurons == -1 ? numInputs : numHiddenNeurons),
        _numHiddenLayers(numHiddenLayers),
        _tolerance(tolerance),
        _epoch(epoch),
        _rho(rho){
    _init();
}

void cBidirPerceptron::_init() {
    if (_inputs) {
        _term();
    }

    printf("Initializing Memory Layout:\n");

    // allocate an array for holding input values. Additional one for bias (like a linear equation)
    _inputs = (double *) malloc(sizeof(double) * (1 + _numInputs));

    // allocate an array for holding output values. _numOutputs can be at least 1.
    _outputs = (double *) malloc(sizeof(double) * _numOutputs);

    // allocate an array for hidden layers
    // This array will contain an array of hidden neuron values per layer.
    // extra one is for bias
    _hiddenLayers = (double **) malloc(sizeof(double *) * _numHiddenLayers);

    // Similarly another array of layers for holding weights of the hidden neurons
    // we will add one more for hidden layer which will hold weights for output layer.
    // input -w-> hn -w-> hn -w-> output: so when there are two layer of hidden neurons, there are three layers
    // of weights
    _hiddenLayerWeights = (double ***) malloc(sizeof(double **) * (_numHiddenLayers + 1));

    size_t outerSize, innerSize;    // looks like array[outerSize][innerSize] in the multi-dim array.
    long l = 0;
    do
    {
        if (l==0)
        {
            outerSize = _numHiddenNeurons;
            innerSize = _numInputs + 1;
            _hiddenLayers[l] = (double*)malloc(sizeof(double) * (1 + _numHiddenNeurons));
            _hiddenLayers[l][_numHiddenNeurons] = 1.;
        }
        else if (l==_numHiddenLayers)
        {
            outerSize = _numOutputs;
            innerSize = _numHiddenNeurons + 1;
        }
        else
        {
            outerSize = _numHiddenLayers;
            innerSize = _numHiddenLayers + 1;
            _hiddenLayers[l] = (double*)malloc(sizeof(double) * innerSize);
            _hiddenLayers[l][_numHiddenNeurons] = 1.;
        }
        _hiddenLayerWeights[l] = (double**)malloc(sizeof(double*) * outerSize);
        for (long o=0; o<outerSize; o++)
        {
            _hiddenLayerWeights[l][o] = (double*)malloc(sizeof(double) * innerSize);
            // initialize with random weights
            for (long i=0; i < innerSize; i++)
            {
                _hiddenLayerWeights[l][o][i] = RAND_WEIGHT;
            }
        }
    } while (++l < _numHiddenLayers + 1);
}

void cBidirPerceptron::_term() {
    // TODO: There may be still a memory leak. Double check.
    if (_inputs) {
        free(_inputs);
        free(_outputs);
        size_t outerSize;    // looks like array[outerSize][innerSize] in the multi-dim array.
        long l = 0;
        do {
            if (l == 0) {
                outerSize = _numHiddenNeurons;
                free(_hiddenLayers[l]);
            } else if (l == _numHiddenLayers+1) {
                outerSize = _numOutputs;
            } else {
                outerSize = _numHiddenLayers;
                free(_hiddenLayers[l]);
            }
            for (long o=0; o<outerSize; o++)
                free(_hiddenLayerWeights[l][o]);
            free(_hiddenLayerWeights[l]);
        } while (++l < _numHiddenLayers + 1);
        free(_hiddenLayerWeights);
        free(_hiddenLayers);
        _inputs = nullptr;
        _outputs = nullptr;
        _hiddenLayers = nullptr;
        _hiddenLayerWeights = nullptr;
    }
}

cBidirPerceptron::~cBidirPerceptron() {
    //_term();
}

cBidirPerceptron &cBidirPerceptron::Train(size_t rows, const double &(*getCell)(size_t row, size_t col),
                                          const ACTIVATION_FUNC_TYPE &activationFunction,
                                          const ERROR_FUNC_TYPE& errorFunction) {

    _activationFunction = activators[(size_t) activationFunction - 1][0];
    _activationDervFunction = activators[(size_t) activationFunction - 1][1];
    _errorFunction = errorFunctions[(size_t)errorFunction - 1];

    _fGetCell = getCell;
    for (long r = 0; r < rows; r++) {
        double error;
        int currentiter = 0;
        do {
            // set input data
            _setInputData(r);
            // feed forward and set outputs
            _feedForwardLayers();
            // back propagate
            _backpropagateLayers(r);
            // get the error
            error = _calculateError(r);
            // feed forward again until error is equal to or less than tolerance.
            currentiter++;
        } while (error > _tolerance && _epoch > currentiter);
    }

    return *this;
}

void cBidirPerceptron::_setInputData(size_t row) {
    for (long c = 0; c < _numInputs; c++) {
        // set inputs
        double ival = _fGetCell(row, c);
        _inputs[c] = ival;
    }
    _inputs[_numInputs] = 1.0; // bias
}

void cBidirPerceptron::_feedForwardLayers() {
    // Feed forward
    // a. Summation of [input * weight]
    // b. Sigmoid activation step
    // for each hidden neuron, go through all connected
    // input (if layer=0) or the last hidden layer neurons
    // apply the linear equation (summation) + computer sigmoid value
    // set the result to the output neuron.

    printf("Feed Forwarding Layers:\n");

    _feedForwardLayer(0); //recursively start with layer 0
}

void cBidirPerceptron::_feedForwardLayer(size_t layer) {

    if (layer > _numHiddenLayers) return;

    printf("Layer: %zd\n", layer);
    if (layer == 0) // activate from input to first hidden layer.
    {
        // the total numHiddenNeurons is a matrix product of number of layer0 hidden neurons and inputs
        for (long h=0; h<_numHiddenNeurons; h++)
        {
            _hiddenLayers[0][h] = 0.;
            for (long i=0; i<_numInputs+1; i++)
            {
                double &weight = _hiddenLayerWeights[0][h][i];
                double &input = _inputs[i];
                auto prod = weight * input;
                _hiddenLayers[0][h] += prod;
            }
            auto actval = _activationFunction(_hiddenLayers[0][h]);
            _hiddenLayers[0][h] = actval;
        }
        PRINTMEM(_hiddenLayers[layer], _numHiddenNeurons + 1);
    }
    else if (layer == _numHiddenLayers) // activate layer final to outputs
    {
        for (long o=0; o<_numOutputs; o++)
        {
            _outputs[o] = 0.;
            for (long h0=0; h0<_numHiddenNeurons + 1; h0++)
            {
                double &weight = _hiddenLayerWeights[_numHiddenLayers][o][h0];
                double &h0val = _hiddenLayers[_numHiddenLayers-1][h0];
                _outputs[0] += (weight * h0val);
            }
            _outputs[0] = _activationFunction(_outputs[0]);
        }
        PRINTMEM(_outputs, _numOutputs);
    }
    else // activate from layer-1 to layer-0
    {
        for (long h=0; h<_numHiddenNeurons; h++)
        {
            _hiddenLayers[layer][h] = 0.;
            for (long h0=0; h0<_numHiddenNeurons + 1; h0++)
            {
                double &weight = _hiddenLayerWeights[layer][h][h0];
                double &h0val = _hiddenLayers[layer-1][h0];
                _hiddenLayers[layer][h] += (weight * h0val);
            }
            _hiddenLayers[layer][h] = _activationFunction(_hiddenLayers[layer][h]);
        }
        PRINTMEM(_hiddenLayers[layer], _numHiddenNeurons + 1);
    }
    _feedForwardLayer(++layer);
}

void cBidirPerceptron::_backpropagateLayers(size_t row) {
    printf("Beginning to back propate from output through hidden to input.");

    // create layers to hold error values
    // layer 0 (between input and hidden)
    // last layer to hold output error to expected value.
    // penultimate layer to hold error between last hidden layer to output neurons
    // between layers to hold error between hidden neurons in each layer.

    const long idxFirstHiddenLayer = 0;
    const long idxFinalHiddenLayer = _numHiddenLayers;

    double* errOutput = (double*)malloc(sizeof(double ) * _numOutputs);
    double **errHiddenLayer = new double*[_numHiddenLayers + 1]; // (double **) malloc(sizeof(double *) * _numHiddenLayers + 1);

    // allocate last layer for output errors
    // compute output error to expected value.
    for (long o=0; o<_numOutputs; o++)
    {
        // expected output - computed output * derivative of (computed output)
        const double &oval = _outputs[o];
        const double &expectedVal = _fGetCell(row, _numInputs);
        const double &adjVal = _activationDervFunction(oval);
        const double result = expectedVal - oval * adjVal;
        errOutput[o] = result;
    }

    // compute hidden value error to output error value.
    errHiddenLayer[idxFinalHiddenLayer] = (double*)malloc(sizeof(double) * _numHiddenNeurons);
    for (long h=0; h<_numHiddenNeurons;h++)
    {
        errHiddenLayer[idxFinalHiddenLayer][h] = 0.;
        for (long o=0; o<_numOutputs;o++)
        {
            const double &dblOutputError = errOutput[o];
            const double &dblHiddenWeight = _hiddenLayerWeights[_numHiddenLayers][o][h];
            errHiddenLayer[idxFinalHiddenLayer][h] += dblOutputError * dblHiddenWeight;
        }
        const double result = _activationDervFunction(_hiddenLayers[_numHiddenLayers-1][h]);
        errHiddenLayer[idxFinalHiddenLayer][h] *= result;
    }

    // move from last layer to first layer and
    // obtain hidden neuron errors.
    long layer = idxFinalHiddenLayer - 1;
    while (layer >= idxFirstHiddenLayer)
    {

        // first calculate the error (h1-h0)

        // make adjustments
        errHiddenLayer[layer] = (double*)malloc(sizeof(double) * _numHiddenNeurons);
        for (long h0=0; h0<_numHiddenNeurons; h0++)
        {
            errHiddenLayer[layer][h0] = 0.;
            for (long h1=0; h1<_numHiddenNeurons; h1++)
            {
                const double &dblErrorsNextHiddenNeuron = errHiddenLayer[layer+1][h1];
                const double &dblHiddenWeight = _hiddenLayerWeights[layer][h1][h0];
                errHiddenLayer[layer][h0] += dblErrorsNextHiddenNeuron * dblHiddenWeight;
            }
            const double result = _activationDervFunction(_hiddenLayers[layer][h0]);
            errHiddenLayer[layer][h0] *= result;
        }
        layer--;
    }

    layer = 0;

    // we reached the first hidden layer and obtained the error factor per layer from output.
    // Use the error factor and readjust weights
    // from input to first hidden layer weights, between subsequent hidden layer weights,
    // and finally between final hidden layer to output layer weights.

    // first input to first hidden layer
    size_t idx = 0;
    size_t idxTo ;
    for (long h=0; h<_numHiddenNeurons; h++)
    {
        for (long i=0; i<_numInputs+1; i++) {
            const double& inputVal = _inputs[i];
            const double& herror = errHiddenLayer[idx][h];
            const double result = _rho * herror * inputVal;
            _hiddenLayerWeights[idx][h][i] += result;
        }
        PRINTMEM(_hiddenLayerWeights[idx][h], _numHiddenNeurons + 1);
    }

    // then from one hidden to another
    idx = 1;
    idxTo = _numHiddenLayers;
    for (long l = idx; l < idxTo; l++) {
        for (long h = 0; h < _numHiddenNeurons; h++) {
            for (long h0 = 0; h0 < _numHiddenNeurons + 1; h0++) {
                // hiddenWeights[h][h0] += _rho * herror * h0val
                const double h0val = _hiddenLayers[l-1][h0];
                const double herror = errHiddenLayer[l][h];
                const double result = _rho * herror * h0val;
                _hiddenLayerWeights[l][h][h0] += result;
            }
            PRINTMEM(_hiddenLayerWeights[l][h], _numHiddenNeurons + 1);
        }
    }

    // and last hidden to output
    idx = _numHiddenLayers;
    for (long o=0; o<_numOutputs; o++) {
        for (long h0 = 0; h0 < _numHiddenNeurons + 1; h0++) {
            const double hlayerval = _hiddenLayers[idx-1][h0];
            const double oerror = errHiddenLayer[idx][o];
            const double result = _rho * oerror * hlayerval;
            _hiddenLayerWeights[idx][o][h0] += result;
        }
        PRINTMEM(_hiddenLayerWeights[idx][o], _numHiddenNeurons + 1);
    }

    for (long pos=0; pos<= _numHiddenLayers; pos++) {
        free(errHiddenLayer[pos]);
    }

    delete [] errHiddenLayer;
    free(errOutput);
}

double cBidirPerceptron::_calculateError(size_t row) {

    // The input data record has [numInputs] + [output value]
    // Hence to get actual output value, we use _numInputs as index
    long o;
    double error = 0.0;
    for (o = 0; o < _numOutputs; o++)
    {
        const double& realOutput = _fGetCell(row, _numInputs);
        const double& estimatedOutput = _outputs[o];
        error += _errorFunction(realOutput, estimatedOutput);
    }
    return _numOutputs ? error / (double)o : 0.;
}

cBidirPerceptron &cBidirPerceptron::Reset() {
    _term();
    _init();
    return *this;
}

cBidirPerceptron &cBidirPerceptron::Predict(double *dataToPredict) {

    if (dataToPredict)
    {
        for (long i=0; i<_numInputs; i++)
        {
            _inputs[i] = dataToPredict[i];
        }
        _inputs[_numInputs] = 1.0;
    }

    _feedForwardLayers();

    return *this;
}

const double cBidirPerceptron::getPredictedResult() {
    double maxVal = _outputs[0];
    for (long i = 1; i < _numOutputs; i++)
    {
        if (_outputs[i] > maxVal)
        {
            maxVal = _outputs[i];
        }
    }
    return maxVal;
}


const double &cBidirPerceptron::getError() {
    return _error;
}
