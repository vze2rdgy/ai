//
// Created by santony on 8/21/20.
//

#ifndef CLANG_MATHFUNCTIONS_H
#define CLANG_MATHFUNCTIONS_H

#include <stdlib.h>
#include <time.h>

#define RANDINIT()	srand(time(NULL))

#define RANDOM()	((float)rand() / (float)RAND_MAX)

#define RANDMAX(x)	(int)((float)(x)*rand()/(RAND_MAX+1.0))

#define PI (double)3.1415926

// https://en.wikipedia.org/wiki/Sigmoid_function
// Sigmoid exponential.
double SigmoidE(const double& x);

// Derivative of Sigmoid exponential.
double SigmoidED(const double& x);

// Tangent function.
double Tangent(const double& x);

// Tangent derivative function.
double TangentD(const double& x);

// softmax (https://en.wikipedia.org/wiki/Softmax_function)


// below are various error functions.
// :param actualValue: the value we know.
// :param estimatedValue: the value estimated by the learner.
const double SquaredError(const double& expectedValue, const double& estimatedValue);

//----- distance functions ---//
const double EuclideanDistance(const double& x0, const double& x1);


#endif //CLANG_MATHFUNCTIONS_H
