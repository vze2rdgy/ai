//
// Created by santony on 12/18/20.
//

#ifndef CLANG_KMEANS_H
#define CLANG_KMEANS_H


class KMeans {

public:
    // A record in the data provided by the caller. Each record expects a maximum of '_cFeatures' features and
    // its grouping classification.
    // Example
    // Features | group
    // 4, 5, 5  | 1
    // 3, 4, 1  | 2
    // so on.
    typedef struct
    {
        int group;
        double * features;
    } FeatureRecord;

public:
    /*
     * Create an instance.
     * @param featureCount: int - The number of features in the data vector.
     * @param kclusters: int - The number of clusters defined by kmeans algorithm. This is an initial value.
     *      If autoCluster = true, this value may change internally.
     * @param autoCluster: bool - Set this to true for intelligent clustering beyound kclusters value set (yet to implement).
    */
    KMeans(const int featureCount, const int kclusters = 3, const bool autoCluster = false);
    /*
     * Destructor.
     */
    ~KMeans();

    /*
     * Pass in the training records.
     * @param records: A two dimensional array of records. It can be created like below example by the caller
     * using Record = KMeans::FeatureRecord;
     * ...
     * ..
     * constexpr int cFeatures = 3;
     * constexpr int cRecords = 10;
     * Record records[cRecords]);
     *  for (auto x=0; x<cRecords; x++)
        {
            records[x].group = RANDMAX(k);
            for (auto y=0; y<cFeatures; y++) {
                records[x].features = new double[cFeatures];
                records[x].features[y] = (double) (RANDMAX(cRecords) + 1);
                printf("%.2f ", records[x].features[y]);
            }
            printf(" | %d (%s)\n", records[x].group, Groups[records[x].group]);
        }
     */
    const KMeans& Train(FeatureRecord* records, long numberOfRecords) ;
    const KMeans& Predict(const FeatureRecord& record) const;
    /*
     * Obtain the class assigned to the giver user provided record's index.
     * Returns -1 if class cannot be obtained (ensure that recordIndex is between zero and
     * one less than the size of data records.
     */
    const double getCluster(long recordIndex) const;
    const double getPredictedValue() const;


private:
    void _Term(); // internal clean up function.
    void _initKMeans();
    void _assignCentroids(int cluster);
    long _detectNearestCentroid(long recordIndex);
    double _measureDistance(long recordIndex, long clusterIndex);
    void _beginClustering();

private:
    int _k; // # of clusters. defaults to three. But will automatically increase this value for further optimization.
    int _cFeatures; // number of attributes in the feature fectors. Classification is based of features.
    bool _autoCluster ;
    double** _centroids = nullptr; // the kmeans detected centroids are stored in this two-dim array.
    long _cRecords = 0; // number of records in the _records array (see below)
    FeatureRecord* _records = nullptr;
    double* _classes = nullptr; // internally stores associated cluster of each member in _records.
    double _predictedCluster = 0.;
};


#endif //CLANG_KMEANS_H
