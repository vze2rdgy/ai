//
// Created by santony on 8/20/20.
//

#ifndef CLANG_CBIDIRPERCEPTRON_H
#define CLANG_CBIDIRPERCEPTRON_H

#include "cDataMatrix.h"

/*
 * The type of activation function used by forward
 * propagation.
 */
enum class ACTIVATION_FUNC_TYPE
{
    // Sigmoid simple [ 1/(1+e^-x) ]
    SIGMOID = 1,
    // Hyperbolic tangent [ (e^x - e^-x) / (e^x + e^-x) ]
    TANGENT = 2,
};

enum class ERROR_FUNC_TYPE
{
    // Mean Squared Error
    MSE = 1,
};

typedef double (*FACTIVATOR)(const double&);
typedef const double (*FERROR)(const double&, const double&);

class cBidirPerceptron {
public:
    /*
     * Create a multi-layer bidirectional perceptron system.
     * @param numInputs: Number of input neurons.
     * @param numOutputs: Number of output neurons. Defaults to 1.
     * @param numHiddenNeurons: Number of hidden neurons per hidden layer. If a value is
     *      not provided (default=-1), then this value equals number of output neurons.
     *  @param numHiddenLayers: Number of hidden layers, each with numHiddenNeurons number of
     *      hidden neurons.
     *  @param tolerance: The tolerance limit for the error between expected and actual value.
     *      The system will continue to adjust weights until tolerance is reached. Defaults to
     *      0.01 (default implementation utilizes MSE to calculate tolerance).
     */
    cBidirPerceptron(int numInputs, int numOutputs = 1,
            int numHiddenNeurons = -1, int numHiddenLayers = 1,
            double tolerance = 0.01,
            int epoch = 1000,
            const double rho = 0.01);
    cBidirPerceptron(const cBidirPerceptron&) = default;
    cBidirPerceptron(cBidirPerceptron&&) = default;
    ~cBidirPerceptron();

    // The input data is row-column matrix of double values.
    // The number of columns must match numInputs + 1 constructor parameter.
    // The last column is expected to be expected values (y).
    // | x1 | x2 | x3 | y |
    // | x1 | x2 | x3 | y |
    // | x1 | x2 | x3 | y |
    // | x1 | x2 | x3 | y |
    cBidirPerceptron& Train(size_t rows,
                            const double &(*getCell)(size_t row, size_t col),
                            const ACTIVATION_FUNC_TYPE& activationFunction = ACTIVATION_FUNC_TYPE::SIGMOID,
                            const ERROR_FUNC_TYPE& errorFunction = ERROR_FUNC_TYPE::MSE);

    // Reset stored data/weights from previously trained information.
    // Same as creating a new instance of this class.
    cBidirPerceptron& Reset();

    // Predict value. Expected an array of double values of length numInput provided in the constructor.
    cBidirPerceptron& Predict(double* dataToPredict);

    const double& getError();

    const double getPredictedResult();

private:
    // Initialize arrays.
    void _init();

    // free arrays
    void _term();

    // set input data

    void _setInputData(size_t row);

    // feed forward hidden layers
    void _feedForwardLayers();

    // forward feed
    void _feedForwardLayer(size_t layer);

    // back propagate from outputs to inputs
    void _backpropagateLayers(size_t row);

    //void _backpropagateLayer(size_t layer);

    // compare output and expected and measure the error distance.
    double _calculateError(size_t row);

private:
    size_t _numInputs, _numOutputs, _numHiddenNeurons, _numHiddenLayers;
    double _tolerance, _error = 1.0, _epoch;
    FACTIVATOR _activationFunction = nullptr;
    FACTIVATOR _activationDervFunction = nullptr;
    FERROR _errorFunction = nullptr;
    double *_inputs = nullptr, *_outputs = nullptr,
        **_hiddenLayers = nullptr, ***_hiddenLayerWeights = nullptr;
    const double &(*_fGetCell)(size_t row, size_t col);
    const double _rho;
};


#endif //CLANG_CBIDIRPERCEPTRON_H
