//
// Created by sajia on 8/6/2020.
//

#include "cHebbLearn.h"
#include <stdio.h>

cHebbLearn::cHebbLearn(const int& attrcount, const float& learnrate) :
        _learnRate(learnrate),
        _inputcount(attrcount) {
    init();
}

cHebbLearn::~cHebbLearn() {
    cleanup();
}

void cHebbLearn::cleanup() {
    if (_inputcount) {
        if (_weights) {
            for (int i = 0; i < _inputcount; i++) {
                delete[] _weights[i];
            }
            delete[] _weights;
        }
        delete[] _inputs;
        delete[] _outputs;
        _weights = nullptr;
        _outputs = _inputs = nullptr;
        _inputcount = 0;
    }
}

void cHebbLearn::init() {
    // initialize weights to zero
    _inputs = new float[_inputcount];
    _outputs = new float[_inputcount];
    _weights = new float *[_inputcount];
    for (int i = 0; i < _inputcount; i++) {
        _inputs[i] = _outputs[i] = 0.0;
        _weights[i] = new float[_inputcount];
        for (int j = 0; j < _inputcount; j++) {
            _weights[i][j] = 0.0;
        }
    }
}

void cHebbLearn::adjustWeights(float *rec) {
    /*
     * dW(i,j) = e * input[j] * output[i]
     * Notice that every input is connected to each output
     * eg: If there are inputs A and B and outputs x and y
     *  A is connected to x and A is connected to y
     *  B is connected x and B is connected to y
     *  W(A,x) is the weight of relationship between A and x,
     *  W(A,y) is the weight of relationship between A and y,
     *  W(B,x) is the weight of relationship between B and x
     *  W(B,y) is the weight of relationship between B and y
     *  Below code loops to every interconnected cell.
     */
    for (int i = 0; i < _inputcount; i++) {
        _inputs[i] = _outputs[i] = rec[i];
        for (int j = 0; j < _inputcount; j++) {
            _weights[i][j] += _learnRate * _inputs[j] * _outputs[i];
        }
    }
}

void cHebbLearn::fit(float **fitdata, int numrows) {
    for (int i = 0; i < numrows; i++) {
        adjustWeights(fitdata[i]);
    }
}


float *cHebbLearn::predict(float rec[]) {
    for (int i = 0; i < _inputcount; i++) {
        _outputs[i] = 0;
        for (int j = 0; j < _inputcount; j++) {
            _outputs[i] += _weights[i][j] * rec[j];
        }
    }
    return _outputs;
}