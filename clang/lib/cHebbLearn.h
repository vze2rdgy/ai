//
// Created by sajia on 8/6/2020.
//

#ifndef CLANG_CHEBBLEARN_H
#define CLANG_CHEBBLEARN_H

/*
 * Hebbian Learning is a unsupervised learner.
 * In this learner, the weight between an input and an output is adjusted based on
 * how strongly correlated are the input and the output.
 * dW = exy, where x = input, y = output, e - learning rate which is a value between 0.0 and 1.0 and dW is adjusted weight.
 *
 * Hebbian learner is good in recognizing a pattern that closely resembles
 * a pattern available in the data that it has learned.
 *
 * Eg. Learn a picture of deer and then pass different pictures of animals. It could
 * recogonize deer (provided the picture is closely correlated).
 *
 * TODO: Using a cost function, auto adjust by self identifying an optimal learning rate.
 */

class cHebbLearn {

    int _inputcount = 0;
    float** _weights = nullptr;
    float* _inputs = nullptr;
    float* _outputs = nullptr;
    float _learnRate;

public:
    // Create an instance. The learnrate defaults to .2 (e in comments above).
    cHebbLearn(const int& attrcount, const float &learnrate = 0.2);

    cHebbLearn(const cHebbLearn&) = default;
    cHebbLearn(cHebbLearn&&) = default;

    ~cHebbLearn();

    // Fit with training data.
    // inputcount: Number of inputs. Note that for hebbian simple learner, # of outputs equals # of inputs.
    // inputs: An float array of inputs. The size of the array must be equal to inputcount
    void fit(float** fitdata, int numrows);

    // Recogonize a pattern
    float* predict(float rec[]);

private:
    void init();
    // Apply hebbian rule and adjust weights.
    void adjustWeights(float* rec);

    // Clean up used memory.
    void cleanup();
};


#endif //CLANG_CHEBBLEARN_H
