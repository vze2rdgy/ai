//
// Created by santony on 8/21/20.
//

#include "MathFunctions.h"
#include <math.h>

double SigmoidED(const double &x) {
    return (x * (1.0 - x));
}

double SigmoidE(const double &x) {
    return (1.0 / (1.0 + exp(-1 * x)));
}

double Tangent(const double &x) {
    //return (exp(x) - exp(-1 * x)) / (exp(x) + exp(-1 * x));
#if BUILD_SYSTEM == WIN
    return tanh(x);
#else
    return tanhf64x(x);
#endif
}

double TangentD(const double &x) {
    //TODO derivative formula.
#if BUILD_SYSTEM == WIN
    double r = 1. / pow(cosh(x), 2.);
#else
    double r = 1. / pow(coshf64x(x), 2.);
#endif
    return r;
}

const double SquaredError(const double &expectedValue, const double &estimatedValue) {
    return pow(expectedValue - estimatedValue, 2);
}

const double EuclideanDistance(const double &x0, const double &x1) {
    return sqrt(pow((x0 - x1), 2));
}