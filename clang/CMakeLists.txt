cmake_minimum_required(VERSION 3.17)
project(clang)

set(CMAKE_CXX_STANDARD 17)

if(CMAKE_SYSTEM_NAME STREQUAL "Windows")
    set(BUILD_SYSTEM 0)
else()
    set(BUILD_SYSTEM 1)
endif()

configure_file("${PROJECT_SOURCE_DIR}/cmake_config.h.in" "${PROJECT_BINARY_DIR}/config.h")

add_subdirectory("${PROJECT_SOURCE_DIR}/lib" "${PROJECT_SOURCE_DIR}/lib/lib_output")
add_subdirectory("${PROJECT_SOURCE_DIR}/cl" "${PROJECT_SOURCE_DIR}/debug_output")

